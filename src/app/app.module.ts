import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//modulos criados:
//o HttpClientModule é para criar uma injeção de dependencia http no client
//o FormsModule é para importar formularios do html 
//o MatIconModule habilita icones do material
import {HttpClientModule} from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon'; 

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule ,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
