import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//tudo tranquilo aqui
@Injectable({
  providedIn: 'root'
})
export class CepServiceService {

  constructor(private httpClient: HttpClient) { }
  buscar(cep:String){//aqui chama o cep na pagina do viacep como orientado na propria pagina
    return this.httpClient.get(`https://viacep.com.br/ws/${cep}/json`)
  }
}
