import { Component } from '@angular/core';
import { Form } from '@angular/forms';
import { CepServiceService } from './cep-service.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CEPApp';

  
  //construtor para consultar o cep-service.service
  constructor(private cepsService: CepServiceService){}

  //consulta Cep no cep-service.service e preenche o populaForm
  consultaCep(valor:any, form:Form){
    this.cepsService.buscar(valor).subscribe((dados)=>this.populaForm(dados,form));
  }
  //recebe dados em formulario e preenche no html
  populaForm(dados:any,form:any){
    form.setValue({
      cep: dados.cep,
      logradouro: dados.logradouro,
      bairro: dados.bairro,
      cidade: dados.localidade,
      uf: dados.uf
    })
  }
}
